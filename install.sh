#!/bin/sh

cd
curl "https://bitbucket.org/Ushirom/epiconf/raw/master/.epiconf.sh" > .epiconf.sh
curl "https://bitbucket.org/Ushirom/epiconf/raw/master/uninstall.sh" > .uninstall.sh
chmod +x .epiconf.sh
chmod +x .uninstall.sh
cp .bashrc .bashrc.save
echo "./.epiconf.sh" >> .bashrc
